import { SHOW_NOTIF, HIDE_NOTIF } from '../actions/types';

const INITIAL_STATE = {
    open: false,
    variant: 'error',
    message: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SHOW_NOTIF:
            return { open : true, ...action.payload };
        case HIDE_NOTIF:
            return INITIAL_STATE;           
        default:
            return state;
    }
};