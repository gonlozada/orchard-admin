import {
    SET_ADM_SITE_VALUES,
    DEL_ADM_SITE_VALUES
} from '../actions/types';

const INITIAL_STATE = {
    _id: null,
    address: '',
    observations: '',
    lat: -37.77,
    lng: -183.77,
    zoom: 12,
    contactName: '',
    contactPhone: '',
    dialog_open: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_ADM_SITE_VALUES:
            return {
                ...state,
                ...action.payload
            };
        case DEL_ADM_SITE_VALUES:
            return INITIAL_STATE;
        default:
            return state;
    }
};