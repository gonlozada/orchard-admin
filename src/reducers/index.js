import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import authReducer from './auth';
import notifReducer from './notif';
import sitesReducer from './sites';
import admSiteReducer from './adm_site';

export default combineReducers({
    auth: authReducer,
    form: formReducer,
    notif: notifReducer,
    sites: sitesReducer,
    adm_site: admSiteReducer
});