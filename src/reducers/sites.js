import {
    SET_LIST,
    SET_SITE_SELECTED,
    SET_WORK_SELECTED,
    SET_GROUP_SELECTED,
    UPDATE_SITE
} from '../actions/types';

const INITIAL_STATE = {
    list: null,
    site_selected: null,
    work_selected: null,
    group_selected: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_LIST:
            return {
                ...state,
                list: action.payload
            };
        case UPDATE_SITE:
            let ret = { ...state }
            let list = [ ...ret.list ]
            if (ret.site_selected) {
                list[ret.site_selected.index] = action.payload;
            } else {
                let updateSite = list.findIndex(site => site._id === action.payload._id);
                if (updateSite)
                    list[updateSite] = action.payload;
                else
                    list.push(action.payload);

            }

            ret.list = list;

            return ret;
        case SET_SITE_SELECTED:
            return {
                ...state,
                site_selected: action.payload
            };
        case SET_WORK_SELECTED:
            return {
                ...state,
                work_selected: action.payload
            };
        case SET_GROUP_SELECTED:
            return {
                ...state,
                group_selected: action.payload
            };
        default:
            return state;
    }
};