import { SIGN_IN, SIGN_OUT } from '../actions/types';

const INITIAL_STATE = {
    userInfo: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SIGN_IN:
            return { userInfo: { ...state.userInfo, ...action.payload } };
        case SIGN_OUT:
            console.log("reached logout");
            return INITIAL_STATE;
        default:
            return state;
    }
};