import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';
import {
  GroupInfo as GroupInfoView,
  WorkInfo as WorkInfoView,
  SiteInfo as SiteInfoView,
  Sites as SitesView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/sites"
      />

      <RouteWithLayout
        component={GroupInfoView}
        exact
        layout={MainLayout}
        path="/sites/:id/work/:idWork/group/:idGroup"
      />

      <RouteWithLayout
        component={WorkInfoView}
        exact
        layout={MainLayout}
        path="/sites/:id/work/:idWork"
      />

      <RouteWithLayout
        component={SiteInfoView}
        exact
        layout={MainLayout}
        path="/sites/:id"
      />

      <RouteWithLayout
        component={SitesView}
        exact
        layout={MainLayout}
        path="/sites"
      />

      <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path="/settings"
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MainLayout}
        path="/not-found"
      />

      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
