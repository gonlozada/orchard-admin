import * as types from './types';

export const signIn = (profile) => {
    return {
        type: types.SIGN_IN,
        payload: {
            ...profile
        }
    };
};

export const signOut = () => {
    return {
        type: types.SIGN_OUT
    };
};

export const showNotification = (notification) => {
    return {
        type: types.SHOW_NOTIF,
        payload: {
            ...notification
        }
    };
};

export const hideNotification = () => {
    return {
        type: types.HIDE_NOTIF
    };
};

export const setSitesList = sites => {
    return {
        type: types.SET_LIST,
        payload: sites
    };
};

export const updateSite = site => {
    return {
        type: types.UPDATE_SITE,
        payload: site
    };
};

export const setSiteSelected = site_selected => {
    return {
        type: types.SET_SITE_SELECTED,
        payload: site_selected
    };
};

export const setWorkSelected = work_selected => {
    return {
        type: types.SET_WORK_SELECTED,
        payload: work_selected
    };
};
export const setGroupSelected = group_selected => {
    return {
        type: types.SET_GROUP_SELECTED,
        payload: group_selected
    };
};

export const setADMSiteValues = values => {
    return {
        type: types.SET_ADM_SITE_VALUES,
        payload : values
    }
}

export const delADMSiteValues = values => {
    return {
        type: types.DEL_ADM_SITE_VALUES
    }
}