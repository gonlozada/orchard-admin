 export const required = value => value ? undefined : 'Field required';
 export const minLength = min => value => value && value.length < min ? `Must be ${min} characters or more` : undefined;
 export const maxLength = max => value => value && value.length > max ? `Must be ${max} characters or less` : undefined;
 export const email = value => value && !/^[A-Z0-9ñ._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined;
 export const onlyLetters = value => value && !/^[A-Za-zñ\s]+$/.test(value) ? 'Only letters and spaces are allowed' : undefined;
 export const confirmPassword = (value, { password }) => !password ? 'Write a password first' : value !== password ? 'Confirm password not match' : undefined;

