export const fetchApi = (path, result) => {
    fetch(process.env.REACT_APP_API_URL + '/' + path,
    {
      credentials: 'include'
    })
    .then(res => {
      if (res.status === 200) {
        result(res);
      } else {
        res.json().then(res => {
          console.log(res.error);
        });
      }
    })
    .catch(err => {
      console.error(err);
    });
}