import React from 'react';
import { connect } from 'react-redux';
import { GroupCard } from './components';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  }
}));


const Groups = (props) => {
  const { groups } = props;
  const classes = useStyles();

  if (!groups) return null;

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={2}
      >
        {
          groups.map((group, index) => (
            <Grid
              key={group._id}
              item
              lg={2}
              sm={6}
              xl={2}
              xs={12}
            >
              <GroupCard group={group} group_index={index} />
            </Grid>
          ))
        }
      </Grid>
    </div>
  );

};

const mapStateToProps = ({ sites }) => {

  let work_selected_data_groups = sites.work_selected !== null ?
    sites.list[sites.site_selected.index].works[sites.work_selected.index].groups
    :
    null;

  return {
    groups: work_selected_data_groups
  }
}

export default connect(mapStateToProps)(Groups);
