import React from 'react';
import { connect } from 'react-redux';
import { setGroupSelected } from 'actions';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActionArea,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%'
  }
}));

const GroupCard = props => {
  const { group, group_index, setGroupSelected, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root)}
    >
      <CardActionArea onClick={() => setGroupSelected({
        id: group._id,
        index: group_index
      })}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {group.supervisor}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            State: construction...
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default connect(null, { setGroupSelected })(GroupCard);
