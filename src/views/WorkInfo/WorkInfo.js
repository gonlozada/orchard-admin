import React from 'react';
import { connect } from 'react-redux';
import { Groups } from './components';
import { CommonToolbar, CalculateRoute } from 'components';
import { setWorkSelected } from 'actions';

const WorkInfo = (props) => {

  const { setWorkSelected } = props;


  return (
    <div>
      <CalculateRoute currentView={WorkInfo.name} />
      <CommonToolbar backAction={() => setWorkSelected(null)} />
      <Groups />
    </div>
  );

};

export default connect(null, { setWorkSelected })(WorkInfo);
