import React, { useState } from 'react';
import AvatarEditor from 'react-avatar-editor';
import {
    Slider,
    Grid,
    Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    grid: {
        background: 'rgb(26, 26, 26)',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    }
}));

const ImageEditor = ({ imageSource, setImageEdited }) => {
    const [imageScale, setImageScale] = useState(1);
    const [imageRotate, setImageRotate] = useState(0);
    const classes = useStyles();
    return (
        <div>
            <Grid
                container
                className={classes.grid}
                justify="center"
                alignItems="center"
            >
                <AvatarEditor
                    image={imageSource}
                    width={250}
                    height={250}
                    border={0}
                    borderRadius={125}
                    color={[0, 0, 0, .6]}
                    scale={imageScale}
                    rotate={imageRotate}
                    ref={setImageEdited}
                />
            </Grid>
            <Typography id="slider-scale">Zoom</Typography>
            <Slider
                value={imageScale}
                min={1}
                max={3}
                step={.05}
                onChange={(e, value) => setImageScale(parseFloat(value))}
                aria-labelledby="slider-scale"
                track={false}
                valueLabelDisplay="auto"
            />
            <Typography id="slider-rotate">Rotate</Typography>
            <Slider
                value={imageRotate}
                min={-45}
                max={45}
                step={1}
                onChange={(e, value) => setImageRotate(parseFloat(value))}
                aria-labelledby="slider-rotate"
                track={false}
                valueLabelDisplay="auto"
            />
        </div>
    )
};

export default ImageEditor;
