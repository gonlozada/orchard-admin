import React, { useState } from 'react';
import { showNotification, signIn } from 'actions';
import { connect } from 'react-redux';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { ImageEditor } from './components';

const useStyles = makeStyles(theme => ({
  input: {
    display: 'none'
  }
}));

const ProfilePicture = (props) => {
  const [imageSource, setImageSource] = useState(null);
  const [imageRef, setImageRef] = useState(null);

  const classes = useStyles();

  const handleImageUpload = event => {
    let [file] = event.target.files;
    if (file) {
      var reader = new window.FileReader();
      reader.onload = (e) => {
        setImageSource(e.target.result);
      }
    }
    reader.readAsDataURL(file);
  };

  const setImageEdited = imageRef => setImageRef(imageRef);

  const uploadImage = () => {
    if (imageRef !== null) {
      const imageUrl = imageRef.getImageScaledToCanvas().toDataURL();
      const imageBase64 = imageUrl.split(',')[1];
      fetch(process.env.REACT_APP_API_URL + '/updateprofilepicture', {
        method: 'POST',
        body: JSON.stringify({ imageBase64: imageBase64 }),
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include'
      })
        .then(res => {
          if (res.status === 200) {
            res.json()
              .then(res => {
                props.showNotification({
                  variant: 'success',
                  message: res.response
                });
              });
          } else {
            res.json()
              .then(res => {
                props.showNotification({
                  variant: 'error',
                  message: res.response
                });
              }).catch(err => {
                console.error(err);
              });
          }
        })
        .catch(err => {
          console.error(err);
        });
    }
  };

  return (
    <Card>
      <CardHeader
        title="Profile picture"
      />
      <Divider />
      <CardContent>
        <input
          type="file"
          id="contained-button-file"
          accept="image/*"
          multiple={false}
          onChange={handleImageUpload}
          className={classes.input}
        />
        <label htmlFor="contained-button-file">
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            startIcon={<CloudUploadIcon />}
            component="span"
          >
            Upload
          </Button>
        </label>
        {
          imageSource &&
            <ImageEditor
              imageSource={imageSource}
              setImageEdited={setImageEdited}
            />
        }
      </CardContent>
      <Divider />
      <CardActions>
        <Button
          color="primary"
          variant="outlined"
          onClick={uploadImage}
          disabled={!imageRef}
        >
          Update picture
        </Button>
      </CardActions>
    </Card>
  )
}

const mapStateToProps = ({ auth }) => {
  let ret = {
    userInfo: auth.userInfo
  }
  return ret;
}

export default connect(mapStateToProps, { showNotification, signIn })(ProfilePicture);