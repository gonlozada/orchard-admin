import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { showNotification, signIn } from 'actions';
import { connect } from 'react-redux';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button,
  TextField,
  Box
} from '@material-ui/core';
import * as validate from 'common/validators';

class Password extends React.Component {
  renderInput = ({ input, label, meta, type }) => {
    const { error, touched } = meta;
    const errorMsg = error && touched ? error : null;
    const errorFlag = error && touched ? true : false;
    return (
      <TextField
        {...input}
        fullWidth
        helperText={errorMsg}
        error={errorFlag}
        label={label}
        variant="outlined"
        autoComplete="off"
        type={type}
      />
    );
  }

  onSubmit = (formValues) => {
    fetch(process.env.REACT_APP_API_URL + '/updatepassword', {
      method: 'POST',
      body: JSON.stringify(formValues),
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
      .then(res => {
        if (res.status === 200) {
          res.json()
            .then(res => {
              this.props.showNotification({
                variant: 'success',
                message: res.response
              });
              this.props.signIn(formValues);
            });
        } else {
          res.json()
            .then(res => {
              this.props.showNotification({
                variant: 'error',
                message: res.response
              });
            }).catch(err => {
              console.error(err);
            });
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    const { pristine, submitting } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Card>
          <CardHeader
            title="Password"
          />
          <Divider />
          <CardContent>
                <Field
                  name="password"
                  component={this.renderInput}
                  label={'New password'}
                  type="password"
                  validate={[validate.required, (value) => validate.minLength(4)(value), (value) => validate.maxLength(10)(value)]}
                />
                <Box mt="1rem">
                  <Field
                    name="confirmPassword"
                    component={this.renderInput}
                    label={'Confirm password'}
                    type="password"
                    validate={[validate.required, validate.confirmPassword]}
                  />
                </Box>
          </CardContent>
          <Divider />
          <CardActions>
            <Button
              color="primary"
              type="submit"
              variant="outlined"
              disabled={this.props.invalid || pristine || submitting}
            >
              Update password
            </Button>
          </CardActions>
        </Card>
      </form>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  let ret = {
    userInfo: auth.userInfo
  }
  return ret;
}

export default connect(mapStateToProps, { showNotification, signIn })(
  reduxForm({
    form: 'password',
    enableReinitialize: true
  })(Password)
);