import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { showNotification, signIn } from 'actions';
import { connect } from 'react-redux';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import * as validate from 'common/validators';

class AccountDetails extends React.Component {
  renderInput = ({ input, label, meta, type }) => {
    const { error, touched } = meta;
    const errorMsg = error && touched ? error : null;
    const errorFlag = error && touched ? true : false;
    return (
      <TextField
        {...input}
        fullWidth
        helperText={errorMsg}
        error={errorFlag}
        label={label}
        margin="dense"
        variant="outlined"
        autoComplete="off"
        type={type}
      />
    );
  }

  onSubmit = (formValues) => {
    fetch(process.env.REACT_APP_API_URL + '/updateUser', {
      method: 'POST',
      body: JSON.stringify(formValues),
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
      .then(res => {
        if (res.status === 200) {
          res.json()
            .then(res => {
              this.props.showNotification({
                variant: 'success',
                message: res.response
              });
              this.props.signIn(formValues);
            });
        } else {
          res.json()
            .then(res => {
              this.props.showNotification({
                variant: 'error',
                message: res.response
              });
            }).catch(err => {
              console.error(err);
            });
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    const { pristine, submitting } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Card>
          <CardHeader
            subheader={this.props.userInfo.email}
            title="Account details"
          />
          <Divider />
          <CardContent>
            <Grid
              container
              spacing={3}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <Field
                  name="firstName"
                  component={this.renderInput}
                  label={'First name'}
                  validate={[validate.required, validate.onlyLetters]}
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <Field
                  name="familyName"
                  component={this.renderInput}
                  label={'Family name'}
                  validate={[validate.required, validate.onlyLetters]}
                />
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
          <CardActions>
            <Button
              color="primary"
              type="submit"
              variant="outlined"
              disabled={this.props.invalid || pristine || submitting}
            >
              Update account
            </Button>
          </CardActions>
        </Card>
      </form>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  let ret = {
    userInfo: auth.userInfo,
    initialValues: {
      firstName: auth.userInfo.firstName,
      familyName: auth.userInfo.familyName
    }
  }
  return ret;
}

export default connect(mapStateToProps, { showNotification, signIn })(
  reduxForm({
    form: 'accountDetails',
    enableReinitialize: true
  })(AccountDetails)
);