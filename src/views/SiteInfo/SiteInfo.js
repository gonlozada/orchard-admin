import React from 'react';
import { SiteDetails, WorksTable } from './components';
import { SiteDialog } from 'components/dialogs';
import { connect } from 'react-redux';
import { CommonToolbar, CalculateRoute } from 'components';
import { setSiteSelected } from 'actions';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  }
}));


const SiteInfo = (props) => {
  const { setSiteSelected } = props;
  const classes = useStyles();


  return (
    <div>
      <CalculateRoute currentView={SiteInfo.name} />
      <CommonToolbar backAction={() => setSiteSelected(null)} title="Back to sites" />
      <div className={classes.root}>
        <SiteDialog />
        <SiteDetails />
        <WorksTable />
      </div>
    </div>
  );

};

export default connect(null, { setSiteSelected })(SiteInfo);
