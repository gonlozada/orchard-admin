import React from 'react';
import { connect } from 'react-redux';
import { setADMSiteValues } from 'actions';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Button,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Paper
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import Location from './components/Location';

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(2)
  },
  header: {
    textAlign: 'center'
  },
  content: {
    padding: 0,
    '&:last-child': {
      paddingBottom: 0,
    }
  },
  paper: {
    height: '100%',
    minHeight: '15em',
    padding: theme.spacing(1)
  }
}));

const SiteDetails = props => {
  const { className, site, setADMSiteValues, ...rest } = props;

  const classes = useStyles();

  if (!site) return null;

  const handleEditDetails = () => {
    setADMSiteValues({
      dialog_open: true,
      ...site
    });
  }

  return (
    <Grid
      className={classes.root}
      container
      spacing={2}
    >

      <Grid
        item
        lg={6}
        sm={12}
        xl={6}
        xs={12}
      >
        <Card
          {...rest}
          className={clsx(className)}
        >
          <CardHeader
            className={classes.header}
            action={
              <Button
                color="primary"
                size="small"
                variant="outlined"
                onClick={handleEditDetails}
              >
                <EditIcon fontSize="small" /> EDIT DETAILS
          </Button>
            }
            title="Site details"
          />
          <Divider />
          <CardContent className={classes.content}>
            <PerfectScrollbar>
              <div className={classes.inner}>
                <Table>
                  <TableBody>

                    <TableRow>
                      <TableCell>
                        <Typography variant="h6">Address</Typography>
                      </TableCell>
                      <TableCell>{site.address}</TableCell>
                    </TableRow>

                    <TableRow>
                      <TableCell>
                        <Typography variant="h6">Observations</Typography>
                      </TableCell>
                      <TableCell>{site.observations}</TableCell>
                    </TableRow>


                    <TableRow>
                      <TableCell>
                        <Typography variant="h6">Contact Name</Typography>
                      </TableCell>
                      <TableCell>{site.contactName}</TableCell>
                    </TableRow>

                    <TableRow>
                      <TableCell>
                        <Typography variant="h6">Contact Phone</Typography>
                      </TableCell>
                      <TableCell>{site.contactPhone}</TableCell>
                    </TableRow>
                  </TableBody>


                </Table>
              </div>
            </PerfectScrollbar>
          </CardContent>
        </Card>
      </Grid>
      <Grid
        item
        lg={6}
        sm={12}
        xl={6}
        xs={12}
      >
        <Paper className={classes.paper}>
          <Location center={{ lat: site.lat, lng: site.lng }} zoom={site.zoom} />
        </Paper>
      </Grid>
    </Grid>
  );
};


const mapStateToProps = ({ sites }) => {
  let site_selected_data =
    (sites.site_selected !== null) ?
      sites.list[sites.site_selected.index]
      :
      null;

  return {
    site: site_selected_data
  }
}

export default connect(mapStateToProps, {setADMSiteValues})(SiteDetails);
