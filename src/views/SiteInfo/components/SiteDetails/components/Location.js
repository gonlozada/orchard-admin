import React from 'react';
import { compose, withProps } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const Location = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCpYVO6p-F3-KDYm2rKRFbBEEOIUh1tVcI&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
)((props) => {

    return (

        <GoogleMap
            defaultZoom={props.zoom}
            center={props.center}
            options={{ streetViewControl: false }}
        >
            <Marker position={props.center} />
        </GoogleMap>
    )
}
);

export default Location;