import React, { useState } from 'react';
import { connect } from 'react-redux';
import { setWorkSelected, showNotification } from 'actions';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Divider,
  Card,
  CardHeader,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  Typography
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { VdmMenu } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  header: {
    textAlign: 'center'
  },
  content: {
    padding: 0,
    '&:last-child': {
      paddingBottom: 0,
    }
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  tableRow: {
    cursor: 'pointer'
  }
}));

const WorksTable = props => {
  const { className, works, work_selected, setWorkSelected, showNotification, ...rest } = props;

  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorWork, setAnchorWork] = useState(null);

  const classes = useStyles();

  if (!works) return null;


  const handleMoreClick = (event, work) => {
    setAnchorEl(event.currentTarget);
    setAnchorWork(work)
    event.stopPropagation();
  }

  const handleClose = () => {
    setAnchorEl(null);
  }

  const handleViewClick = () => {
    setWorkSelected({
      id: anchorWork._id,
      index: anchorWork.index
    });
  }
  const handleEditClick = () => {
    underConstruction();
  }
  const handleDeleteClick = () => {
    underConstruction();
  }

  const underConstruction = () => {
    showNotification({
      variant: 'warning',
      message: 'Button under construction'
    })
  }

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        className={classes.header}
        action={
          <Button
            color="primary"
            size="small"
            variant="outlined"
            onClick={underConstruction}
          >
            <AddIcon fontSize="small" /> NEW WORK
          </Button>
        }
        title="Works registered"
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>

              <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>State</TableCell>
                  <TableCell>Groups</TableCell>
                  <TableCell size="small" />
                </TableRow>
              </TableHead>

              <TableBody>

                {works.map((work, index) => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={work._id}
                    onClick={() => setWorkSelected({
                      id: work._id,
                      index: index
                    })}
                  >
                    <TableCell>
                      <div className={classes.nameContainer}>
                        <Typography variant="body1">{work.date}</Typography>
                      </div>
                    </TableCell>
                    <TableCell>{work.description}</TableCell>
                    <TableCell>{work.state}</TableCell>
                    <TableCell>{work.groups.length}</TableCell>
                    <TableCell size="small" padding="none" align="right">
                      <IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={(event) => handleMoreClick(event, { _id: work._id, index: index })}
                      >
                        <MoreVertIcon />
                      </IconButton>

                    </TableCell>
                  </TableRow>
                ))}

              </TableBody>


            </Table>
          </div>
        </PerfectScrollbar>
        <VdmMenu
          anchorEl={anchorEl}
          handleClose={handleClose}
          handleViewClick={handleViewClick}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
        />
      </CardContent>
    </Card>
  );
};


const mapStateToProps = ({ sites }) => {
  let site_selected_data =
    (sites.site_selected !== null) ?
      sites.list[sites.site_selected.index].works
      :
      null;

  return {
    works: site_selected_data,
    work_selected: sites.work_selected
  }
}

export default connect(mapStateToProps, { setWorkSelected, showNotification })(WorksTable);
