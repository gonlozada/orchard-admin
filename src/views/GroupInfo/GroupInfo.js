import React from 'react';
import { connect } from 'react-redux';
import { setGroupSelected } from 'actions';
import { CommonToolbar, CalculateRoute } from 'components';
import { Supervisor, CurrentMembers, TimeSheets } from './components';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 0
  }
}));


const GroupInfo = ({ setGroupSelected }) => {

  const classes = useStyles();


  return (
    <div className={classes.root}>
      <CalculateRoute currentView={GroupInfo.name} />
      <CommonToolbar backAction={() => setGroupSelected(null)} />
      <Supervisor />
      <CurrentMembers />
      <TimeSheets />
    </div>
  );

};

const mapStateToProps = ({ sites }) => {
  return {
    group_selected: sites.group_selected
  }
}

export default connect(mapStateToProps, { setGroupSelected })(GroupInfo);
