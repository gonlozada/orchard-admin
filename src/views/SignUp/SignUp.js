import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { withStyles } from "@material-ui/core/styles";
import { Link as RouterLink, withRouter } from 'react-router-dom';
import { showNotification } from 'actions';
import { connect } from 'react-redux';
import {
    IconButton,
    Link,
    Typography,
    Grid,
    Button,
    TextField
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import * as validate from 'common/validators';

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    quote: {
      backgroundColor: theme.palette.neutral,
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundImage: 'url(/images/auth.jpg)',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center'
    },
    quoteContainer: {
        [theme.breakpoints.down('md')]: {
            display: 'none'
        }
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBototm: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'center'
        }
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
        [theme.breakpoints.down('sm')]: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    title: {
        marginBottom: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    signUpButton: {
        margin: theme.spacing(2, 0)
    }
});



class SignUp extends React.Component {
    handleBack = () => {
        this.props.history.goBack();
    };

    renderInput = ({ input, label, meta, type }) => {
        const { error, touched } = meta;
        const errorMsg = error && touched ? error : null;
        const errorFlag = error && touched ? true : false;
        return (
            <TextField
                {...input}
                fullWidth
                helperText={errorMsg}
                error={errorFlag}
                label={label}
                margin="dense"
                variant="outlined"
                autoComplete="off"
                type={type}
            />
        );
    }

    onSubmit = (formValues) => {
        fetch(process.env.REACT_APP_API_URL + '/register', {
            method: 'POST',
            body: JSON.stringify(formValues),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'omit'
        })
            .then(res => {
                if (res.status === 200) {
                    res.json()
                        .then(res => {
                            this.props.history.push('/sign-in');
                            this.props.showNotification({
                                variant: 'success',
                                message: res.response
                            });
                        });
                } else {
                    res.json()
                        .then(res => {
                            this.props.showNotification({
                                variant: 'error',
                                message: res.response
                            });
                        });
                }
            })
            .catch(err => {
                console.error(err);
            });
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid
                    className={classes.grid}
                    container
                >
                    <Grid
                        className={classes.quoteContainer}
                        item
                        lg={5}
                    >
                        <div className={classes.quote}>
                            <div className={classes.quoteInner}>
                                <Typography
                                    className={classes.quoteText}
                                    variant="h1"
                                >
                                   
                                </Typography>
                                <div className={classes.person}>
                                    <Typography
                                        className={classes.name}
                                        variant="body1"
                                    >
                                        
                                    </Typography>
                                    <Typography
                                        className={classes.bio}
                                        variant="body2"
                                    >
                                        
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </Grid>
                    <Grid
                        className={classes.content}
                        item
                        lg={7}
                        xs={12}
                    >
                        <div className={classes.content}>
                            <div className={classes.contentHeader}>
                                <IconButton onClick={this.handleBack}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </div>
                            <div className={classes.contentBody}>
                                <form
                                    onSubmit={this.props.handleSubmit(this.onSubmit)}
                                    className={classes.form}
                                >
                                    <Typography
                                        className={classes.title}
                                        variant="h2"
                                    >
                                        Sign Up
                                    </Typography>
                                    
                                    <Field
                                        name="firstName"
                                        component={this.renderInput}
                                        label={'First name'}
                                        validate={[validate.required, validate.onlyLetters, (value) => validate.maxLength(30)(value)]}
                                    />
                                    <Field
                                        name="familyName"
                                        component={this.renderInput}
                                        label={'Family name'}
                                        validate={[validate.required, validate.onlyLetters, (value) => validate.maxLength(30)(value)]}
                                    />

                                    <Field
                                        name="email"
                                        component={this.renderInput}
                                        label={'Email address'}
                                        type="email"
                                        validate={[validate.required, validate.email, (value) => validate.maxLength(50)(value)]}
                                    />
                                    <Field
                                        name="password"
                                        component={this.renderInput}
                                        label={'Password'}
                                        type="password"
                                        validate={[validate.required, (value) => validate.minLength(4)(value), (value) => validate.maxLength(10)(value)]}
                                    />
                                    <Field
                                        name="confirmPassword"
                                        component={this.renderInput}
                                        label={'Confirm password'}
                                        type="password"
                                        validate={[validate.required, validate.confirmPassword]}
                                    />
                                    <Button
                                        className={classes.signUpButton}
                                        color="primary"
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained"
                                        disabled={this.props.invalid}
                                    >
                                        Sign up now
                                    </Button>
                                    <Typography
                                        color="textSecondary"
                                        variant="body1"
                                    >
                                        Have an account?{' '}
                                        <Link
                                            component={RouterLink}
                                            to="/sign-in"
                                            variant="h6"
                                        >
                                            Sign in
                                        </Link>
                                    </Typography>
                                </form>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default connect(null, { showNotification })(withRouter(
    withStyles(styles, { withTheme: true })(reduxForm({
        form: 'signUp'
    })(SignUp))
));
