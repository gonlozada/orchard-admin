import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { SitesTable } from './components';
import { SiteDialog } from 'components/dialogs';
import { CalculateRoute } from 'components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  }
}));

const Sites = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CalculateRoute currentView={Sites.name} />
      <SiteDialog />
      <SitesTable />
    </div>
  );
};

export default Sites;
