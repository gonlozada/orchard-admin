import React, { useEffect } from 'react';
import { setSiteSelected, setSitesList, setADMSiteValues, delADMSiteValues, showNotification } from 'actions';
import { fetchApi } from 'common/functions';
import { connect } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  CircularProgress,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { VdmMenu } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0,
    '&:last-child': {
      paddingBottom: 0,
    }
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  tableRow: {
    cursor: 'pointer'
  },
  icon: {
    marginRight: theme.spacing(1),
    color: theme.palette.icon
  }
}));

const SitesTable = props => {
  const { className, sites_list, site_selected, setSiteSelected, setSitesList, setADMSiteValues, showNotification, delADMSiteValues, ...rest } = props;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorSite, setAnchorSite] = React.useState(null);

  useEffect(() => {
    fetchApi('sites', raw => {
      raw.json().then(res => {
        if (JSON.stringify(res.data) !== JSON.stringify(sites_list))
          setSitesList(res.data);
      });
    });

  }, [setSitesList, sites_list]);

  if (sites_list === null)
    return (
      <div align="center">
        <CircularProgress />
      </div>
    );

  const handleMoreClick = (event, site) => {
    setAnchorEl(event.currentTarget);
    setAnchorSite(site)
    event.stopPropagation();
  }
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleViewClick = () => {
    setSiteSelected({
      id: anchorSite._id,
      index: anchorSite.index
    })
  }

  const handleEditClick = () => {
    setADMSiteValues({ dialog_open: true, ...sites_list[anchorSite.index] });
    handleClose();
  }

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        action={
          <Button
            color="primary"
            size="small"
            variant="outlined"
            onClick={() => {
              delADMSiteValues();
              setADMSiteValues({ dialog_open: true });
            }}
          >
            <AddIcon fontSize="small" /> NEW SITE
          </Button>
        }
        title="Your sites"
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>

              <TableHead>
                <TableRow>
                  <TableCell>Address</TableCell>
                  <TableCell>Manager</TableCell>
                  <TableCell>Phone</TableCell>
                  <TableCell>Works</TableCell>
                  <TableCell>Total Groups</TableCell>
                  <TableCell size="small"></TableCell>
                </TableRow>
              </TableHead>

              <TableBody>

                {
                  sites_list.map((site, index) => {

                    let total_groups = 0;
                    site.works.map(work => total_groups += work.groups.length)

                    return (
                      <TableRow
                        className={classes.tableRow}
                        hover
                        key={site._id}
                        onClick={() => setSiteSelected({
                          id: site._id,
                          index: index
                        })}
                      >
                        <TableCell>{site.address}</TableCell>
                        <TableCell>{site.contactName}</TableCell>
                        <TableCell>{site.contactPhone}</TableCell>
                        <TableCell>{site.works.length}</TableCell>
                        <TableCell>{total_groups}</TableCell>
                        <TableCell size="small" padding="none" align="right">
                          <IconButton
                            aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            onClick={(event) => handleMoreClick(event, { _id: site._id, index: index })}
                          >
                            <MoreVertIcon />
                          </IconButton>

                        </TableCell>
                      </TableRow>)
                  })
                }

              </TableBody>


            </Table>

          </div>
        </PerfectScrollbar>
      </CardContent>
      <VdmMenu
        anchorEl={anchorEl}
        handleClose={handleClose}
        handleViewClick={handleViewClick}
        handleEditClick={handleEditClick}
        handleDeleteClick={() =>
          showNotification({
            variant: 'warning',
            message: 'Button under construction'
          })}
      />
    </Card>
  );

};

SitesTable.propTypes = {
  className: PropTypes.string
};

const mapStateToProps = ({ sites }) => {
  return {
    sites_list: sites.list,
    site_selected: sites.site_selected
  }
}

export default connect(mapStateToProps, { setSiteSelected, setSitesList, setADMSiteValues, showNotification, delADMSiteValues })(SitesTable);
