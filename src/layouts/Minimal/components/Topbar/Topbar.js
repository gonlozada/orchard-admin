import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import EcoIcon from '@material-ui/icons/EcoRounded';

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: 'none'
  },
  barTitle: {
    display: 'flex',
    color: theme.palette.white,
    fontWeight: 500,
    fontSize: '15px',
    letterSpacing: '+0.15px',
    alignItems: 'center',
    fontFamily: 'Sedgwick Ave'
  },
  icon: {
    marginRight: '0.15em',
    fontSize: '28px'
  }
}));

const Topbar = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      color="primary"
      position="fixed"
    >
      <Toolbar>
        <RouterLink to="/">
          <Typography className={classes.barTitle}>
            <EcoIcon className={classes.icon} />
            ORCHARD ADMIN
          </Typography>
        </RouterLink>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;
