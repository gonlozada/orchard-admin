import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Hidden, IconButton, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import EcoIcon from '@material-ui/icons/EcoRounded';
import { Logout } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none'
  },
  flexGrow: {
    flexGrow: 1
  },
  barTitle: {
    display : 'flex',
    color: theme.palette.white,
    fontWeight: 500,
    fontSize: '15px',
    letterSpacing: '+0.15px',
    alignItems: 'center',
    fontFamily: 'Sedgwick Ave'
  },
  icon: {
    marginRight: '0.15em',
    fontSize : '35px'
  },

}));

const Topbar = props => {
  const { className, onSidebarOpen, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Toolbar>
        <RouterLink to="/">
          <Typography className={classes.barTitle}>
            <EcoIcon className={classes.icon} />
            ORCHARD ADMIN
          </Typography>
        </RouterLink>
        <div className={classes.flexGrow} />
        <Logout />
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onSidebarOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

export default Topbar;