import React, { useState } from 'react';
import { connect } from 'react-redux';
import { signOut } from 'actions';
import { Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { fetchApi } from 'common/functions'

const Logout = props => {
    const [getRedirect, setRedirect] = useState(false);
    const { signOut } = props;
    const handleLogout = () => {
        fetchApi('logout', () => {
            setRedirect(true);
            signOut();
        });
    }


    if (getRedirect)
        return <Redirect to="/sign-in" />;

    return (
        <Button
            color="inherit"
            onClick={handleLogout}
        >
            LOGOUT
        </Button>
    );
}


export default connect(null, { signOut })(Logout);