import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 90,
    height: 90
  },
  name: {
    marginTop: theme.spacing(1),
    wordBreak: 'break-word',
    textAlign: 'center'
  }
}));

const Profile = props => {
  const { className, userInfo, updateAvatar } = props;

  const classes = useStyles();

  const user = {
    name: userInfo.firstName + ' ' + userInfo.familyName,
    avatar: '/images/avatars/' + userInfo.email + '.png?' + Date.now() + updateAvatar,
    email: userInfo.email
  };

  return (
    <div
      className={clsx(classes.root, className)}
    >
      <Avatar
        alt="Person"
        className={classes.avatar}
        component={RouterLink}
        src={user.avatar}
        to="/settings"
      />
      <Typography
        className={classes.name}
        variant="h4"
      >
        {user.name}
      </Typography>
      <Typography variant="body2">{user.email}</Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};


export default Profile;
