import React from 'react';
import { connect } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import DescriptionIcon from '@material-ui/icons/Description';
import WorkIcon from '@material-ui/icons/Work';
import AccountIcon from '@material-ui/icons/AccountBox';
import ReceiptIcon from '@material-ui/icons/Receipt';
import SettingsIcon from '@material-ui/icons/Settings';
import PlaceIcon from '@material-ui/icons/Place';

import { Profile, SidebarNav } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, userInfo, dispatch, updateAvatar, ...rest } = props;

  const classes = useStyles();

  const pages = [
    {
      title: 'Work',
      href: '/work',
      icon: <WorkIcon />
    },
    {
      title: 'Sites',
      href: '/sites',
      icon: <PlaceIcon />
    },
    {
      title: 'Employees',
      href: '/employees',
      icon: <PeopleIcon />
    },
    {
      title: 'Payslips',
      href: '/payslips',
      icon: <ReceiptIcon />
    },
    {
      title: 'Reports',
      href: '/reports',
      icon: <DescriptionIcon />
    },
    {
      title: 'Account',
      href: '/account',
      icon: <AccountIcon />
    },
    {
      title: 'Settings',
      href: '/settings',
      icon: <SettingsIcon />
    }
  ];

  const pages_filtered = userInfo ? pages.filter(({ href }) => userInfo.access.includes(href)) : null;

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Profile userInfo={userInfo} updateAvatar={updateAvatar} />
        <Divider className={classes.divider} />
        <SidebarNav
          className={classes.nav}
          pages={pages_filtered}
        />
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return { userInfo: state.auth.userInfo, updateAvatar : state.notif.message }
}

export default connect(mapStateToProps)(Sidebar);
