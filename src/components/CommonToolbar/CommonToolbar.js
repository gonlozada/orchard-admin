import React from 'react';
import { IconButton } from '@material-ui/core';
import ArrowBack from '@material-ui/icons/ArrowBack';

const CommonToolbar = (props) => {
    const { backAction } = props;

    return (
        <div >
            <IconButton
                onClick={backAction}
            >
                <ArrowBack />
            </IconButton>

        </div>
    )
}

export default CommonToolbar;