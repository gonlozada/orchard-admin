import React from 'react';

import { makeStyles } from '@material-ui/styles';
import {
    Menu,
    MenuItem
} from '@material-ui/core';

import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles(theme => ({
    root: {},
    icon: {
        marginRight: theme.spacing(1),
        color: theme.palette.icon
    }
}));

const VdmMenu = props => {
    const classes = useStyles();
    const open = Boolean(props.anchorEl);

    return (
        <Menu
            anchorEl={props.anchorEl}
            keepMounted
            open={open}
            onClose={props.handleClose}

        >
            <MenuItem onClick={props.handleViewClick}>
                <VisibilityIcon className={classes.icon} />View details
            </MenuItem>
            <MenuItem onClick={props.handleEditClick}>
                <EditIcon className={classes.icon} /> Edit
            </MenuItem>
            <MenuItem onClick={props.handleDeleteClick}>
                <DeleteIcon className={classes.icon} /> Delete
            </MenuItem>
        </Menu>
    )

}

export default VdmMenu;