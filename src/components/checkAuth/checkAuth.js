import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { signIn } from 'actions';

export default function checkAuth(ComponentTarget, initialRedirectValue) {

  return connect(null, { signIn })(class extends Component {
    state = {
      loading: true,
      redirect: initialRedirectValue
    };


    componentDidMount() {

      fetch(process.env.REACT_APP_API_URL + '/getUserInfo',
        {
          credentials: 'include'
        })
        .then(res => {
          if (res.status === 200) {
            res.json().then(res => {
              this.props.signIn(res);
              this.setState({ loading: false, redirect: !initialRedirectValue });
            })
          } else {
            res.json().then(res => {
              console.log(res.error);
              this.setState({ loading: false });
            })
          }
        })
        .catch(err => {
          console.error(err);
        });
    }


    render() {
      const { loading, redirect } = this.state;

      const redirectTo = initialRedirectValue ? '/sign-in' : '/dashboard';

      if (loading)
        return (
          <div align="center">
            <CircularProgress />
          </div>
        );

      if (redirect)
        return <Redirect to={redirectTo} />;


      return <ComponentTarget {...this.props} />
    }
  })
}