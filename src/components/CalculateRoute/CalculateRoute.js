import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const CalculateRoute = (props) => {
    const { currentView, site_selected, work_selected, group_selected } = props;

    const calculateRoute = () => {

        if (site_selected)
            if (work_selected)
                if (group_selected)
                    return currentView === 'GroupInfo' ? null : '/sites/' + site_selected.id + '/work/' + work_selected.id + '/group/' + group_selected.id;
                else
                    return currentView === 'WorkInfo' ? null : '/sites/' + site_selected.id + '/work/' + work_selected.id;
            else
                return currentView === 'SiteInfo' ? null : '/sites/' + site_selected.id;
        else
            return currentView === 'Sites' ? null : '/sites';

    }
    const route = calculateRoute();
    
    if (route) {
        //console.log('Rendering a redirect to:' + route)
        return <Redirect to={route} />
    } else {
        //console.log('Not redirecting ' + currentView);
        return null;
    }
}

const mapStateToProps = ({ sites }) => {
    return {
        site_selected: sites.site_selected,
        work_selected: sites.work_selected,
        group_selected: sites.group_selected
    }
}
export default connect(mapStateToProps)(CalculateRoute);