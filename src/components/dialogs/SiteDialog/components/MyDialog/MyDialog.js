import React from 'react';
import { connect } from 'react-redux';
import { setADMSiteValues } from 'actions';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Slide,
  useMediaQuery
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  title: {
    color: theme.palette.white,
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  body: {
    margin: theme.spacing(2),
    align: 'center',
    height: '100%'
  },
  button: {
    marginLeft: theme.spacing(2)
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const MyDialog = (props) => {
  const classes = useStyles();
  const { handleSave, setADMSiteValues } = props;
  const [page, setPage] = props.page;
  const smallViewport = useMediaQuery('(min-width:960px)');
  return (
    <Dialog
      fullScreen={!smallViewport}
      open={props.open}
      onClose={() => setADMSiteValues({ dialog_open: false })}
      TransitionComponent={Transition}
      PaperProps={
        page === 2 ?
          {
            style: {
              minWidth: '90%',
              height: '100%'
            },
          } : {}
      }
    >
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton edge="start" color="inherit" onClick={() => setADMSiteValues({ dialog_open: false })} aria-label="close">
            <CloseIcon />
          </IconButton>

          {
            page === 1 ?
              (
                <React.Fragment>
                  <Typography variant="h6" className={classes.title}>
                    {props.title} SITE DETAILS
                  </Typography>
                  <Button autoFocus color="inherit" onClick={() => setPage(2)}>
                    NEXT
                  </Button>
                </React.Fragment>
              )
              :
              (
                <React.Fragment>
                  <Typography variant="h6" className={classes.title}>
                    SELECT LOCATION
                  </Typography>
                  <Button autoFocus color="inherit" className={classes.button} onClick={() => setPage(1)}>
                    BACK
                  </Button>
                  <Button autoFocus color="inherit" className={classes.button} onClick={handleSave}>
                    SAVE
                  </Button>
                </React.Fragment>
              )
          }
        </Toolbar>
      </AppBar>
      <div className={classes.body}>
        {props.children}
      </div>
    </Dialog>
  );
}

export default connect(null, { setADMSiteValues })(MyDialog);