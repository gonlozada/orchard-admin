import React, { useState } from 'react';
import { connect } from 'react-redux';
import { MyMap, MyDialog, SiteDetails } from './components';
import { showNotification, updateSite, setADMSiteValues } from 'actions';

const SiteDialog = (props) => {
  const [page, setPage] = useState(1);
  const { values, showNotification, updateSite, setADMSiteValues } = props;


  const handleSave = () => {
    const url = values._id ? '/sites/' + values._id : '/sites';

    fetch(
      process.env.REACT_APP_API_URL + '/' + url,
      {
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST',
        credentials: 'include'
      })
      .then(res => {

        if (res.status === 200) {
          res.json()
            .then(res => {
              showNotification({
                variant: 'success',
                message: res.message
              });
              setADMSiteValues({ dialog_open: false });
              updateSite(res.data);
            });
        } else {
          res.json()
            .then(res => {
              showNotification({
                variant: 'error',
                message: res.message
              });
            });
        }
        
      })
      .catch(err => {
        console.error(err);
      });
  }

  return (
    <MyDialog open={values.dialog_open} title={values._id ? "EDIT" : "INSERT"} handleSave={handleSave} page={[page, setPage]}>
      {
        page === 1 ?
          <SiteDetails /> : <MyMap />
      }
    </MyDialog>
  );
}
const mapStateToProps = ({ adm_site }) => ({ values: { ...adm_site } });
export default connect(mapStateToProps, { showNotification, updateSite, setADMSiteValues })(SiteDialog);
