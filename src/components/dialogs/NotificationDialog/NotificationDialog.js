import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContentWrapper from './components/SnackbarContentWrapper';
import { connect } from 'react-redux';
import { hideNotification } from 'actions';

const NotificationDialog = (props) => {

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    props.hideNotification();
  };

  if (!props.open)
    return null;
  else
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={ true }
        autoHideDuration={ 2000 + props.message.length * 50 }
        onClose={ handleClose }
      >
        <SnackbarContentWrapper
          onClose={ handleClose }
          variant={ props.variant }
          message={ props.message }
        />
      </Snackbar>
    );
}

NotificationDialog.propTypes = {
  variant: PropTypes.string,
  message: PropTypes.string
};

const mapStateToProps = ({ notif }) => {
  return {
    ...notif
  }
}

export default connect(mapStateToProps, { hideNotification })(NotificationDialog);