import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { setADMSiteValues } from 'actions';
import { compose, withProps } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import RoomIcon from '@material-ui/icons/Room';

const MyMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCpYVO6p-F3-KDYm2rKRFbBEEOIUh1tVcI&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  const mapRef = useRef(null);
  const { setADMSiteValues, center, zoom } = props;
  let delay = null;

  const handleDragEnd = () => {
    if (!mapRef.current) return;
    clearTimeout(delay);
    const center = mapRef.current.getCenter().toJSON();
    setADMSiteValues({
      ...center
    });

  }

  const handleCenterChanged = () => {
    if (!mapRef.current) return;
    clearTimeout(delay);
    const center = mapRef.current.getCenter().toJSON();
    delay = setTimeout(() => {
      setADMSiteValues({
        ...center
      });
    }, 1000);


  }

  const handleZoomChanged = () => {
    if (!mapRef.current) return;
    const zoom = mapRef.current.getZoom();
    setADMSiteValues({
      zoom: zoom
    });

  }

  return (

    <GoogleMap
      ref={mapRef}
      defaultZoom={zoom}
      defaultCenter={center}
      onDragEnd={handleDragEnd}
      onCenterChanged={handleCenterChanged}
      onZoomChanged={handleZoomChanged}
      options={{ streetViewControl: false }}
    >
      <RoomIcon
        style={{
          zIndex: 4,
          position: 'absolute',
          top: '50%',
          left: '50%',
          marginLeft: '-17px'
        }}
        fontSize='large'
      />
    </GoogleMap>
  )
}
);

const mapStateToProps = ({ adm_site }) => {
  return {
    center: {
      lat: adm_site.lat,
      lng: adm_site.lng
    },
    zoom: adm_site.zoom,
  }
}

export default connect(mapStateToProps, { setADMSiteValues })(MyMap);