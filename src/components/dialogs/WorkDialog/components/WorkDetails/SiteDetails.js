import React from 'react';
import { connect } from 'react-redux';
import { setADMSiteValues } from 'actions';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, TextField, Card, CardContent } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: '800px'
    }
}));
const SiteDetails = (props) => {
    const classes = useStyles();

    const { setADMSiteValues, address, observations, contactName, contactPhone, className } = props;
    return (
        <div className={clsx(classes.root, className)}>
            <Grid
                container
                alignContent='flex-start'
                spacing={2}
            >
                <Grid
                    item
                    lg={12}
                    sm={12}
                    xl={12}
                    xs={12}
                >
                    <TextField
                        label="Address"
                        margin="dense"
                        variant="outlined"
                        autoComplete="off"
                        fullWidth="true"
                        onChange={(event) => setADMSiteValues({ address: event.target.value })}
                        value={address}
                    />
                </Grid>
                <Grid
                    item
                    lg={12}
                    sm={12}
                    xl={12}
                    xs={12}
                >
                    <TextField
                        label="Additional information on how to get there"
                        margin="dense"
                        variant="outlined"
                        autoComplete="off"
                        fullWidth="true"
                        multiline="true"
                        rows="6"
                        onChange={(event) => setADMSiteValues({ observations: event.target.value })}
                        value={observations}
                    />
                </Grid>
                <Grid
                    item
                    lg={6}
                    sm={12}
                    xl={6}
                    xs={12}
                >
                    <TextField
                        label="Contact Name"
                        margin="dense"
                        variant="outlined"
                        autoComplete="off"
                        fullWidth="true"
                        onChange={(event) => setADMSiteValues({ contactName: event.target.value })}
                        value={contactName}
                    />
                </Grid>
                <Grid
                    item
                    lg={6}
                    sm={12}
                    xl={6}
                    xs={12}
                >
                    <TextField
                        label="Contact Phone"
                        margin="dense"
                        variant="outlined"
                        autoComplete="off"
                        fullWidth="true"
                        onChange={(event) => setADMSiteValues({ contactPhone: event.target.value })}
                        value={contactPhone}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

const mapStateToProps = ({ adm_site }) => ({
    address: adm_site.address,
    observations: adm_site.observations,
    contactName: adm_site.contactName,
    contactPhone: adm_site.contactPhone
})
export default connect(mapStateToProps, { setADMSiteValues })(SiteDetails);
