const jwt = require('jsonwebtoken');
User = require('./userModel');// Handle index actions
const secret = require('../secret');
const fs = require('fs');

exports.register = (req, res) => {

    const { firstName, familyName, email, password } = req.body;
    const user = new User(
        {
            firstName,
            familyName,
            email,
            password,
            access: [
                '/dashboard',
                '/sites',
                '/settings'
            ]
        });

    user.save(err => {
        if (err) {
            res.status(500)
                .json({
                    response: 'Error registering new user, please try again.'
                });
            console.log(err);
        } else {
            res.status(200)
                .json({
                    response: 'Your account has been created successfully'
                });
        }
    });
}

exports.authenticate = (req, res) => {
    const { email, password } = req.body;
    User.findOne({ email }, function (err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error, please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Incorrect email or password'
                });
        } else {
            user.isCorrectPassword(password, function (err, same) {
                if (err) {
                    res.status(500)
                        .json({
                            error: 'Internal error, please try again'
                        });
                } else if (!same) {
                    res.status(401)
                        .json({
                            error: 'Incorrect email or password'
                        });
                } else {
                    // Issue token
                    const payload = { email };
                    const token = jwt.sign(payload, secret, {
                        expiresIn: '4h'
                    });
                    res.cookie('token', token, { httpOnly: true }).status(200).json({
                        firstName: user.firstName,
                        familyName: user.familyName,
                        email: user.email,
                        access: user.access
                    });
                }
            });
        }
    });
}

exports.logout = (req, res) => {
    res.clearCookie('token', { httpOnly: true }).sendStatus(200);
}

exports.getUserInfo = (req, res) => {
    const { email } = req;
    User.findOne({ email }, function (err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error, please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Invalid email (' + email + ')'
                });
        } else {
            res.status(200).json({
                firstName: user.firstName,
                familyName: user.familyName,
                email: user.email,
                access: user.access
            });
        }
    });
}

exports.updateUser = (req, res) => {
    const { email } = req;
    User.findOne({ email }, function (err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error, please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Invalid email (' + email + ')'
                });
        } else {
            const { firstName, familyName } = req.body;
            user.firstName = firstName;
            user.familyName = familyName;
            user.save(err => {
                if (err) {
                    res.status(500)
                        .json({
                            response: 'Error uploading account, please try again.'
                        });
                } else {
                    res.status(200)
                        .json({
                            response: 'Your account has been uploaded successfully'
                        });
                }
            });
        }
    });
}

exports.updatePassword = (req, res) => {
    const { email } = req;
    User.findOne({ email }, function (err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error, please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Invalid email (' + email + ')'
                });
        } else {
            const { password } = req.body;
            user.password = password;
            user.save(err => {
                if (err) {
                    res.status(500)
                        .json({
                            response: 'Error uploading your password, please try again.'
                        });
                } else {
                    res.status(200)
                        .json({
                            response: 'Your password has been uploaded successfully'
                        });
                }
            });
        }
    });
}
exports.updateProfilePicture = (req, res) => {
    const { email } = req;
    User.findOne({ email }, function (err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error, please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Invalid email (' + email + ')'
                });
        } else {
            const image = req.body.imageBase64;
            fs.writeFile('./public/images/avatars/' + email + '.png', Buffer(image, 'base64'), err => {
                if (err) {
                    console.log(err);
                    res.status(401)
                        .json({
                            response: err
                        });
                } else {
                    console.log('File is created successfully.');
                    res.status(200)
                        .json({
                            response: 'Your picture has been uploaded successfully'
                        });
                }
            });
        }
    });
}