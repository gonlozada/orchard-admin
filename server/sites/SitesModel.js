// SitesModel.js
var mongoose = require('mongoose');// Setup schema

const memebersSchema = new mongoose.Schema({
    id: { type: Number, required: true },
    startTime: { type: String, required: true },
    endTime: { type: String, required: true },
});


const timeSheetsSchema = new mongoose.Schema({
    date: { type: String, required: true },
    description: { type: String, required: true },
    payrate: { type: Number, required: true },
    members: [memebersSchema]
});

const groupsSchema = new mongoose.Schema({
    supervisor: { type: String, required: true },
    currentMembers: { type: Array, required: true },
    timeSheets: [timeSheetsSchema]
});

const worksSchema = new mongoose.Schema({
    date: { type: String, required: true },
    description: { type: String, required: true },
    state: { type: String, required: true },
    // JOB TYPE HERE
    groups: [groupsSchema]
});

const SitesSchema = new mongoose.Schema({
    address: { type: String, required: true },
    observations: { type: String },
    lat: { type: Number, required: true },
    lng: { type: Number, required: true },
    zoom: { type: Number, required: true },
    contactName: { type: String, require: true },
    contactPhone: { type: String, require: true },
    works: [worksSchema]
});// Export Contact model





var Sites = module.exports = mongoose.model('sites', SitesSchema);

module.exports.get = function (callback, limit) {
    Sites.find(callback).limit(limit);
}