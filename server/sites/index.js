// contactController.js
Sites = require('./SitesModel');// Handle index actions
exports.index = function (req, res) {
    Sites.get(function (err, sites) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }

        res.json({
            status: "success",
            message: "sites retrieved successfully",
            data: sites
        });
    });
};
exports.new = function (req, res) {
    var site = new Sites();

    site.address = req.body.address;
    site.observations = req.body.observations;
    site.lat = req.body.lat;
    site.lng = req.body.lng;
    site.zoom = req.body.zoom;
    site.contactName = req.body.contactName;
    site.contactPhone = req.body.contactPhone;
    if (req.body.works) {
        site.works = req.body.works;
    }

    site.save(function (err) {
        if (err)
            res.status(500).json(err);
        else
            res.status(200).json({
                message: 'New site created successfully',
                data : site
            });
    });
};

exports.view = function (req, res) {
    Sites.findById(req.params.site_id, function (err, site) {
        if (err)
            res.json(err);
        else
            res.json({
                message: 'Sites loading..',
                data: site
            });
    });
};

exports.update = function (req, res) {
    Sites.findById(req.params.site_id, function (err, site) {
        if (err)
            res.send(err);
        else {
            site.address = req.body.address;
            site.observations = req.body.observations;
            site.lat = req.body.lat;
            site.lng = req.body.lng;
            site.zoom = req.body.zoom;
            site.contactName = req.body.contactName;
            site.contactPhone = req.body.contactPhone;
            if (req.body.works) {
                site.works = req.body.works;
            }
            
            site.save(function (err) {
                if (err)
                    res.json(err);
                res.json({
                    message: 'Site Info updated',
                    data: site
                });
            });
        }
    });
};


exports.delete = function (req, res) {
    Sites.remove({
        _id: req.params.site_id
    }, function (err, site) {
        if (err)
            res.send(err); res.json({
                status: "success",
                message: 'Site deleted'
            });
    });
};


exports.fill = function (req, res) {

    req.body.address = 'address ' + Math.floor(Math.random() * 100);
    req.body.observations = 'turn left after block 3';
    req.body.lat = -37.77;
    req.body.lng = -183.77;
    req.body.zoom = 12;
    req.body.contactName = 'contact ' + Math.floor(Math.random() * 100);
    req.body.contactPhone = '0273080' + Math.floor(Math.random() * 100);
    req.body.works = [
        {
            date: Math.floor(Math.random() * 29) + '-02-2020',
            description: 'bud thining',
            state: 'pending',
            groups: [
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                },
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                },
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                },
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                },
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            date: Math.floor(Math.random() * 29) + '-02-2020',
            description: 'girdling',
            state: 'done',
            groups: [
                {
                    supervisor: 'Supervisor ' + Math.floor(Math.random() * 100),
                    currentMembers: [0, 1, 2],
                    timeSheets: [
                        {
                            date: Math.floor(Math.random() * 29) + '-02-2020',
                            description: 'Block ' + Math.floor(Math.random() * 100),
                            payrate: '20',
                            members: [
                                {
                                    id: '0',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '1',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                },
                                {
                                    id: '2',
                                    startTime: '08:00',
                                    endTime: '17:00',
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ];

    exports.new(req, res);
}