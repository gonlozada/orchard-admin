const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const routes = require("./routes");
const app = express();


// Configure bodyparser to handle post requests
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended: true
}));

app.use(cookieParser());

app.use(cors({
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200,
    credentials: true
}));
app.options('*', cors());

// Connect to Mongoose and set connection variable
const mongo_uri = 'mongodb://localhost/orchard';

mongoose.connect(mongo_uri, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${mongo_uri}`);
    }
});

// Use Api routes in the App
app.use('/', routes);

// Launch app to listen to specified port
const port = process.env.PORT || 8080;
app.listen(port, function () {
    console.log("Running Server on port " + port);
});