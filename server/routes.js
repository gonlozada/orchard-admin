const withAuth = require('./withAuthentication');
const User = require('./user');

// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', (req, res) => {
    res.json({
        status: 'API is working',
        message: 'Welcome',
    });
});

//User signup ,signin, signout and get profile
router.post('/register', User.register);
router.post('/authenticate', User.authenticate);
router.get('/logout', withAuth, User.logout);
router.get('/getuserinfo', withAuth, User.getUserInfo);

//User settings
router.post('/updateuser', withAuth, User.updateUser);
router.post('/updatepassword', withAuth, User.updatePassword);
router.post('/updateprofilepicture', withAuth, User.updateProfilePicture);

//Encrypted token
router.get('/checkToken', withAuth, (req, res) => {
    res.sendStatus(200);
});

// Import sites controller
var sites = require('./sites');

// sites routes
router.route('/sites')
    .all(withAuth)
    .get(sites.index)
    .post(sites.new);

//Fill the database with random data
router.get('/fillsites', sites.fill);

router.route('/sites/:site_id')
    .all(withAuth)
    .get(sites.view)
    .post(sites.update)
    .delete(sites.delete);

// Export API routes
module.exports = router;